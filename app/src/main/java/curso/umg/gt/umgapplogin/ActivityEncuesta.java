package curso.umg.gt.umgapplogin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

public class ActivityEncuesta extends AppCompatActivity {

    private Spinner sp1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encuesta);

        sp1 = (Spinner) findViewById(R.id.sp1);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.opciones, android.R.layout.simple_spinner_dropdown_item);
        sp1.setAdapter(adapter);


    }

    public void mensaje(View view){
        Toast noti = Toast.makeText(this, "Se guardó sus datos, gracias..", Toast.LENGTH_SHORT);
        noti.show();
    }
}
